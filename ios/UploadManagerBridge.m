//
//  UploadManagerBridge.m
//  AwesomeProject
//
//  Created by patricio on 21/03/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(UploadManager, NSObject)

RCT_EXTERN_METHOD(clickbutton)
RCT_EXTERN_METHOD(deleteFile)

@end
