//
//  UploadManager.swift
//  AwesomeProject
//
//  Created by patricio on 20/03/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
import Photos


@objc(UploadManager)
class UploadManager: NSObject {
  
  @objc func clickbutton() {
    let fileManager = FileManager.default
    let applicationGroupId = "group.testapic.PatricioRecordit"
    //        let groupURL = fileManager.containerURL(forSecurityApplicationGroupIdentifier: applicationGroupId)
    guard let groupURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: applicationGroupId) else {
      fatalError("could not get shared app group directory.")
    }
    var url = groupURL.appendingPathComponent("Library")
    let baseurl = url.appendingPathComponent("Caches")
    url = baseurl.appendingPathComponent("screenCapture.mp4")
    //        let compressurl = baseurl.appendingPathComponent("compress.zip")
    if fileManager.fileExists(atPath: url.path) {
      do {
        let attr = try fileManager.attributesOfItem(atPath: url.path)
        let size = attr[FileAttributeKey.size]!
      }
      catch {
      }
      let present = AVOutputSettingsAssistant(preset: AVOutputSettingsPreset640x480)
      var adf = present?.videoSettings
      let asdf =  adf![AVVideoCompressionPropertiesKey] as! Dictionary<String, Any?>
      print(asdf["ProfileLevel"])
      print(present?.videoSettings)
      print("--------------------")
      print(present?.audioSettings)
      //            savetophotos(url: url)
      PHPhotoLibrary.shared().performChanges({
        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
      }) { completed, error in
        if completed {
          print("Video is saved!")
        }
        else if error != nil {
          print(error!)
        }
      }
    }
    else {
      print("does not exists --------------------------------------------------")
    }
  }
  
  @objc func deleteFile() {
    let fileManager = FileManager.default
    let applicationGroupId = "group.testapic.PatricioRecordit"
    guard let groupURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: applicationGroupId) else {
      fatalError("could not get shared app group directory.")
    }
    var url = groupURL.appendingPathComponent("Library")
    url = url.appendingPathComponent("Caches")
    url = url.appendingPathComponent("screenCapture.mp4")
    
    if fileManager.fileExists(atPath: url.path) {
      do {
        try fileManager.removeItem(at: url)
      }
      catch {
        print("didnt delete=====-------------------")
      }
    }
    else {
      print("didnt delete=====-------------------2")
    }
  }
  
}
