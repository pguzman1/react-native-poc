//
//  SampleHandler.swift
//  RecorderExtension
//
//  Created by patricio on 20/03/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//


import ReplayKit
import os
import AVKit
import AVFoundation
import Photos
import UserNotifications


enum IntParsingError: Error {
  case overflow
  case invalidInput(String)
}

class SampleHandler: RPBroadcastSampleHandler {
  var AW: AVAssetWriter!
  var AWI: AVAssetWriterInput!
  var AWIAudio: AVAssetWriterInput!
  var canIstartWriting: Bool = false
  var isFirstSample: Bool = true
  var isWriting: Bool = false
  var assetWriterInputPixelBufferAdaptor: AVAssetWriterInputPixelBufferAdaptor!
  var dispatch_queue: DispatchQueue!
  var index: Int = 0
  var tests: [String]? = []
  
  
  override func broadcastStarted(withSetupInfo setupInfo: [String : NSObject]?) {
    let ud = UserDefaults.init(suiteName: "group.testapic.PatricioRecordit")
    ud?.addObserver(self, forKeyPath: "index", options: .new ,context: nil)
    let arrayInDefaults = ud!.array(forKey: "mytests")
    let array = arrayInDefaults as? [String]
    tests = array
    startTest()
  }
  
  override func broadcastPaused() {
    os_log("APP GOT PAUSEDDDDDDD")
  }
  
  override func broadcastResumed() {
    os_log("APP GOT RESUMEEEDDDDDD")
  }
  
  func startTest() {
    setNotificationsTimer()
    dispatch_queue = DispatchQueue(label: "com.testapic.PatricioRecordit")
    self.setupAVAssetWriter()
    if self.AW.canAdd(self.AWI) {
      self.AW.add(self.AWI)
      if self.AW.canAdd(self.AWIAudio) {
        self.AW.add(self.AWIAudio)
      }
      else {
        os_log("******************************** CANT ADD AUDIO ")
      }
      let started = self.AW.startWriting()
      if started {
        self.canIstartWriting = true
      }
      else {
        self.canIstartWriting = false
        let _ = self.AW.error.debugDescription
        os_log("************************************** start writing is false")
      }
    }
    else {
      os_log("******************************** CAN T ADD")
    }
    os_log("*********************************** BROADCAST STARTED")
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    guard let key = keyPath else {
      return
    }
    if key == "index" {
      self.index = change![NSKeyValueChangeKey.newKey] as! Int
    }
  }
  
  
  func setNotificationsTimer() {
    let timer = Timer.init(timeInterval: 1, repeats: true, block: { (Timer) in
      UNUserNotificationCenter.current().getDeliveredNotifications { (notif) in
        if notif.isEmpty {
          let content = UNMutableNotificationContent()
          if self.tests != nil {
            if self.tests!.count > 0 {
              content.body = "\(self.tests![self.index])"
            }
            else {
              content.body = "0"
            }
          }
          else {
            content.body = "There are no test"
          }
          content.categoryIdentifier = "tutorial"
          let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
          let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
          UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (pendings) in
            if pendings.isEmpty {
              UNUserNotificationCenter.current().add(request) { (error) in
                if error != nil {
                  let asdf: String = String(describing: error)
                  os_log("------------------------------____THERE IS AN ERROR %@", asdf)
                }
                os_log("asddasdasddsadasdsadasd-----------------------")
              }
            }
            else {
              
            }
          })
        }
        else {
          os_log("----------------------------------------------------NOT EMPTY")
        }
      }
    })
    RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
  }
  
  
  func setupAVAssetWriter() {
    let url = getvideoURL()
    do {
      AW = try AVAssetWriter(outputURL: url, fileType: .mp4)
    }
    catch {
      os_log("------------------------------------------------------erro1")
      return
    }
    //        let _viewSize = UIScreen.main.bounds
    //        let _scale = UIScreen.main.scale
    //        let pixelNumber = _viewSize.width * _viewSize.height * _scale;
    let present = AVOutputSettingsAssistant(preset: .preset640x480)
    //        let videoCompression = [AVVideoAverageBitRateKey: pixelNumber * 11.4]
    //        let videoSettings = [AVVideoCodecKey: AVVideoCodecType.h264, AVVideoWidthKey: _viewSize.width * _scale, AVVideoHeightKey: _viewSize.height * _scale, AVVideoCompressionPropertiesKey: videoCompression] as [String : Any];
    //        var videoSet = present?.videoSettings
    //        var compressionProperties = videoSet![AVVideoCompressionPropertiesKey] as! Dictionary<String, Any?>
    //        compressionProperties["ProfileLevel"] = AVVideoProfileLevelH264Main32          // SHOULD I CHANGE IT, TO TEST IN A 10 MINUTES VIDEO TO SEE SIZE DIFFERENCE
    ////        compressionProperties["AverageBitRate"] = 1000000
    //        videoSet![AVVideoCompressionPropertiesKey] = compressionProperties
    guard AW.canApply(outputSettings: present?.videoSettings, forMediaType: .video) else {
      fatalError("Negative : Can't apply the Output settings...")
    }
    
    guard AW.canApply(outputSettings: present?.audioSettings, forMediaType: .audio) else {
      fatalError("Negative : Can't apply the Output settings...")
    }
    AWI = AVAssetWriterInput(mediaType: .video, outputSettings: present?.videoSettings)          // format descritor???
    AWIAudio = AVAssetWriterInput(mediaType: .audio, outputSettings: present?.audioSettings)
    AWI.expectsMediaDataInRealTime = true
    AWIAudio.expectsMediaDataInRealTime = true
  }
  
  override func broadcastFinished() {
    self.isWriting = false;
    if self.canIstartWriting {
      os_log("*********************************** BROADCAST FINISHED")
      self.AWI.markAsFinished()
      self.AWIAudio.markAsFinished()
      self.AW.finishWriting {
        let asdf = self.AW.error.debugDescription
        os_log("*********************************** BROADCAST FINISHED WRITTING")
        
        switch self.AW.status {
        case .failed:
          os_log(" ********************************************** FAILED")
          os_log("**********************************%@", self.AW.error.debugDescription)
          print(self.AW.error.debugDescription)
          print(asdf.debugDescription)
          break
        case .cancelled:
          os_log(" ********************************************** cancelled")
          break
        case .writing:
          os_log(" ********************************************** writings")
          break
        case .unknown:
          os_log(" ********************************************** unknown")
          break
        case .completed:
          os_log(" ********************************************** completed")
          break
        }
      }
    }
  }
  
  func getvideoURL() -> URL {
    let applicationGroupId = "group.testapic.PatricioRecordit"
    guard let groupURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: applicationGroupId) else {
      fatalError("could not get shared app group directory.")
    }
    let fileManager = FileManager.default
    //        let videoOutputURL = documentDirectory.appendingPathComponent("OutputVideo.mp4") // true .... file://var
    var url = groupURL.appendingPathComponent("Library")
    url = url.appendingPathComponent("Caches")
    url = url.appendingPathComponent("screenCapture.mp4") // file://private
    if fileManager.fileExists(atPath: url.path) {
      do {
        try fileManager.removeItem(at: url)
      }
      catch {
        
      }
    }
    return url
  }
  
  override func processSampleBuffer(_ sampleBuffer: CMSampleBuffer, with sampleBufferType: RPSampleBufferType) {
    if canIstartWriting {
      if CMSampleBufferDataIsReady(sampleBuffer) {
        switch sampleBufferType {
        case RPSampleBufferType.video:
          handleVideoSampleBuffer(sampleBuffer: sampleBuffer)
          break
        case RPSampleBufferType.audioApp:
          break
        case RPSampleBufferType.audioMic:
          handleAudioSampleBuffer(sampleBuffer: sampleBuffer)
          break
        }
      }
    }
  }
  
  func handleVideoSampleBuffer(sampleBuffer: CMSampleBuffer) {
    os_log("*********************************** HANDLE VIDEO")
    
    if (self.isFirstSample) {
      let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
      self.AW.startSession(atSourceTime: timestamp)
      self.isFirstSample = false;
    }
    if CMSampleBufferIsValid(sampleBuffer) && CMSampleBufferDataIsReady(sampleBuffer) {
      var asd = AWI.isReadyForMoreMediaData
      if asd {
        os_log("SAMPLE IS READY AND IM GONNA APPEND")
        AWI.append(sampleBuffer)
      }
      else {
        os_log("SAMPLE IS NOT READY FOR MORE MEDIA DATA")
      }
    }
  }
  
  func handleAudioSampleBuffer(sampleBuffer: CMSampleBuffer) {
    if CMSampleBufferIsValid(sampleBuffer) && CMSampleBufferDataIsReady(sampleBuffer) {
      var asd = AWI.isReadyForMoreMediaData
      if asd {
        os_log("SAMPLE IS READY AND IM GONNA APPEND")
        AWIAudio.append(sampleBuffer)
      }
      else {
        os_log("SAMPLE IS NOT READY FOR MORE MEDIA DATA")
      }
    }
  }
}
