/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import { Container, Header, Content, List, Left, Body, Title, Right, ListItem, Text, Button, Separator, Icon} from 'native-base';
import {
  NativeModules,
  Platform,
  StyleSheet,
  FlatList,
  View,
  NavigatorIOS,
  TouchableOpacity
} from 'react-native';
import Second from './Navigation'
import { StackNavigator } from 'react-navigation';

const instructions = Platform.select({
  ios: 'IOS',
  android: 'ANDROID',
});

// <Header>
//   <Left/>
//   <Body>
//     <Title>Testapic</Title>
//   </Body>
//   <Right>
//     <Button transparent>
//       <Icon name='person' />
//     </Button>
//   </Right>
// </Header>
//

class Home extends Component {
  static navigationOptions = {
  title: 'Testapic',
};
  render() {
    return (
      <View style={styles.container1}>
        <Container>
          <Content>

            <List>
              <Separator bordered>
                <Text>Test non débuté / en cours</Text>
              </Separator>
              <ListItem onPress={() => this.props.navigation.navigate('Second')}>
                <TouchableOpacity>
                  <Text>Test d'essai iOS</Text>
                </TouchableOpacity>
              </ListItem>
              <ListItem onPress={() => {}}>
                <Text>Test Tripadvisor</Text>
              </ListItem>
              <ListItem onPress={() => {}}>
                <Text>Test Mcdo</Text>
              </ListItem>
            </List>
            <Button block onPress={this.doSwift}>
              <Text>
                Extract
              </Text>
            </Button>
            <Button block onPress={this.deleteFile}>
              <Text>
                Delete
              </Text>
            </Button>
          </Content>
        </Container>
      </View>
    );
  }

  navSecond(){
    this.props.navigation.navigate('Second')
  }

  doSwift() {
    NativeModules.UploadManager.clickbutton()
  }

  deleteFile() {
    NativeModules.UploadManager.deleteFile()
  }
}

const styles = StyleSheet.create({
  container1: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  container: {
    flex: 1,
    paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});


const RootStack = StackNavigator(
  {
    Home: {
      screen: Home,
    },
    Second: {
      screen: Second,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
